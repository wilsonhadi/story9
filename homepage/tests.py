from django.test import TestCase, Client, LiveServerTestCase
from django.apps import apps
from django.urls import resolve
from .views import *
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import unittest
import time

# Create your tests here.

class test(TestCase):

    def test_index_url_exists_and_template_used(self):
        found = resolve('/')
        response = Client().get('/')
        self.assertEqual(found.func, index)
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, 'index.html')

class functional_test(LiveServerTestCase):

    def setUp(self) :
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')

    def tearDown(self):
        self.driver.quit()

    def test_search_have_results(self):
        self.driver.get(self.live_server_url)
        self.driver.find_element_by_id("search").send_keys("harry")
        self.driver.find_element_by_id("searchBtn").click()
        time.sleep(2)
        result = self.driver.find_element_by_id("result0")
        self.assertIn("Harry", result.text)

    def test_increment_like_button_(self):
        self.driver.get(self.live_server_url)
        self.driver.find_element_by_id("search").send_keys("harry")
        self.driver.find_element_by_id("searchBtn").click()
        time.sleep(2)
        self.driver.find_element_by_id("thumbs0").click()
        count = self.driver.find_element_by_id("likes0")
        self.assertEqual("1", count.text)
        count2 = self.driver.find_element_by_id("likes1")
        self.assertEqual("0", count2.text)
    
    def test_sort_and_modal_update(self):
        self.driver.get(self.live_server_url)
        self.driver.find_element_by_id("search").send_keys("harry")
        self.driver.find_element_by_id("searchBtn").click()
        time.sleep(2)
        self.driver.find_element_by_id("thumbs0").click()
        self.driver.find_element_by_id("thumbs0").click()
        self.driver.find_element_by_id("thumbs1").click()
        self.driver.find_element_by_id("modalBtn").click()
        time.sleep(1)
        first = self.driver.find_element_by_xpath("//li[1]")
        second = self.driver.find_element_by_xpath("//li[2]")
        self.assertIn("2 like(s)", first.text)
        self.assertIn("1 like(s)", second.text)